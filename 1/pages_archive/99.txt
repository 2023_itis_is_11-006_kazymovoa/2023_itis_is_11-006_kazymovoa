Advertisement
Department of Electrical Engineering & Computer Science, University of California, Berkeley, Berkley, USA
You can also search for this editor in
PubMed
Google Scholar
Fundamentals of Electrical Engineering, TU Dresden, Dresden, Germany
You can also search for this editor in
PubMed
Google Scholar
Institute of Mathematics and Informatics, Bulgarian Academy of Sciences, Sofia, Bulgaria
You can also search for this editor in
PubMed
Google Scholar
11k Accesses
21
Citations
1
Altmetric
Tax calculation will be finalised at checkout
This is a preview of subscription content, log in via an institution to check for access.
This contributed volume offers practical solutions and design-, modeling-, and implementation-related insights that address current research problems in memristors, memristive devices, and memristor computing. The book studies and addresses related challenges in and proposes solutions for the future of memristor computing. State-of-the-art research on memristor modeling, memristive interconnections, memory circuit architectures, software simulation tools, and applications of memristors in computing are presented. Utilising contributions from numerous experts in the field, written in clear language and illustrated throughout, this book is a comprehensive reference work.
Memristor Computing Systems explains memristors and memristive devices in an accessible way for graduate students and researchers with a basic knowledge of electrical and control systems engineering, as well as prompting further research for more experienced academics.
Leon O. Chua
Ronald Tetzlaff
Angela Slavova
Leon Chua received his MSc degree from the MIT in 1961 and his Ph.D. in 1964 from the University of Illinois at Champaign-Urbana. He became an Assistant Professor of Electrical Engineering at Purdue University in 1964, and was promoted to Associate Professor in 1967. He joined the UC Berkeley in 1970. He is the first recipient of the 2005 Gustav Kirchhoff Award, the highest IEEE Technical Field Award for outstanding contributions to the fundamentals of any aspect of electronic circuits and systems that has a long term significance or impact. He was also awarded the prestigious IEEE Neural Networks Pioneer Award in 2000 for his contributions in neural networks. He was elected a Fellow of the IEEE in 1974 and has received many international prizes, including the IEEE Browder J. Thompson Memorial Prize in 1972, the IEEE W. R. G. Baker Prize in 1978, the Frederick Emmons Award in 1974, the M. E. Van Valkenhurg Award in 1995, and again in 1998. He was awarded 7 USA patents and 8 Honorary doctorates (Doctor Honoris Causa) from major European universities and Japan. He is also a recipient of the top 15 cited authors in Engineering award in 2002, chosen from the Current Contents (ISI) database of all cited papers in the engineering disciplines in the citation index from 1991 to October 31, 2001, from all branches of engineering. He was elected a foreign member of the European Academy of Sciences (Academia Europea) in 1997.
Ronald Tetzlaff is a Full Professor of Fundamentals of Electrical Engineering at the Technische Universität Dresden, Germany. His scientific interests include problems in the theory of signals and systems, stochastic processes, physical fluctuation phenomena, system modelling, system identification, Volterra systems, Cellular Nonlinear Networks, and Memristive Systems. From 1999 to 2003 Ronald Tetzlaff was Associate Editor of the IEEE, Transactions on Circuits and Systems: part I. He was "Distinguished Lecturer" of the IEEE CAS Society (2001-2002). He is a member of the scientific committee of different international conferences. He was the chair of the 7th IEEE International Workshop on Cellular Neural Networks and their Applications (CNNA 2002), of the 18th IEEE Workshop on Nonlinear Dynamics of Electronic Systems (NDES 2010), of the 5th International Workshop on Seizure Prediction (IWSP 2011) and of the 21st European Conference on Circuit Theory and Design (ECCTD 2013). Ronald Tetzlaff is in the Editorial Board of the International Journal of Circuit Theory and Applications since 2007 and he is also in the Editorial Board of the AEÜ - International Journal of Electronics and Communications since 2008. He serves as a reviewer for several journals and for the European Commission. From 2005 to 2007 he was the chair of the IEEE Technical Committee Cellular Neural Networks & Array Computing. He is a member of the Informationstechnische Gesellschaft (ITG) and the German Society of Electrical Engineers and of the German URSI Committee.
Angela Slavova graduated from Technical University, Russe, Computer Engineering, M. Sc. in 1986. In the period 1992-1993 she got Fulbright Scholarship at Florida Institute of Technology, Florida, USA. She got her Ph.D. in Mathematics in 1994. In 2005 she became Doctor of Science and in 2007 Full Professor at the Institute of Mathematics and Informatics, Bulgarian Academy of Sciences. Since 2004 Prof. Slavova is a Head of the Department of Mathematical Physics, Institute of Mathematics, Bulgarian Academy of Sciences. She participated in more than 30 conferences, workshops and seminars as an invited speaker. She got in the period January-July 1998 - CNR Fellowship, University of Florence, Italy. She was visiting professor at the University of Ioannina, Greeece, University of Catania, Italy, University of Torino, Italy, Astronomical Observatory, Torino, Italy, College of Judea and Samaria, Ariel, Israel, University of Ferrara, Italy, University of Bologna, University of Florence, Italy, Ben-Gurion University, Israel, etc. Professor Slavova has more than 100 publications in prestigious journals in Applied Mathematics, IEEE Journals, etc. She is an author and co-author of 3 monographs. Prof. Slavova is a member of AMS, SIAM, Chair of Bulgarian Section of SIAM, Board of Bulgarian Section of WSEAS, EMS, IEEE Technical Committee on CNNAD.
Book Title: Memristor Computing Systems
Editors: Leon O. Chua, Ronald Tetzlaff, Angela Slavova
DOI: https://doi.org/10.1007/978-3-030-90582-8
Publisher: Springer Cham
eBook Packages: Engineering, Engineering (R0)
Copyright Information: Springer Nature Switzerland AG 2022
Hardcover ISBN: 978-3-030-90581-1Published: 24 June 2022
Softcover ISBN: 978-3-030-90584-2Published: 25 June 2023
eBook ISBN: 978-3-030-90582-8Published: 23 June 2022
Edition Number: 1
Number of Pages: XX, 298
Number of Illustrations: 23 b/w illustrations, 132 illustrations in colour
Topics: Circuits and Systems, Computer Communication Networks, Control Structures and Microprogramming, Optical and Electronic Materials
Policies and ethics
Tax calculation will be finalised at checkout
94.180.167.37
Not affiliated
© 2024 Springer Nature