We read every piece of feedback, and take your input very seriously.
To see all available qualifiers, see our documentation.
Redis is an in-memory database that persists on disk. The data model is key-value, but many different kind of values are supported: Strings, Lists, Sets, Sorted Sets, Hashes, Streams, HyperLogLogs, Bitmaps.
This README is just a fast quick start document. You can find more detailed documentation at redis.io.
Redis is often referred to as a data structures server. What this means is that Redis provides access to mutable data structures via a set of commands, which are sent using a server-client model with TCP sockets and a simple protocol. So different processes can query and modify the same data structures in a shared way.
Data structures implemented into Redis have a few special properties:
Another good example is to think of Redis as a more complex version of memcached, where the operations are not just SETs and GETs, but operations that work with complex data types like Lists, Sets, ordered data structures, and so forth.
If you want to know more, this is a list of selected starting points:
Redis can be compiled and used on Linux, OSX, OpenBSD, NetBSD, FreeBSD.
We support big endian and little endian architectures, and both 32 bit
and 64 bit systems.
It may compile on Solaris derived systems (for instance SmartOS) but our
support for this platform is best effort and Redis is not guaranteed to
work as well as in Linux, OSX, and *BSD.
It is as simple as:
To build with TLS support, you'll need OpenSSL development libraries (e.g.
libssl-dev on Debian/Ubuntu) and run:
To build with systemd support, you'll need systemd development libraries (such
as libsystemd-dev on Debian/Ubuntu or systemd-devel on CentOS) and run:
To append a suffix to Redis program names, use:
You can build a 32 bit Redis binary using:
After building Redis, it is a good idea to test it using:
If TLS is built, running the tests with TLS enabled (you will need tcl-tls
installed):
Redis has some dependencies which are included in the deps directory.
make does not automatically rebuild dependencies even if something in
the source code of dependencies changes.
When you update the source code with git pull or when code inside the
dependencies tree is modified in any other way, make sure to use the following
command in order to really clean everything and rebuild from scratch:
This will clean: jemalloc, lua, hiredis, linenoise and other dependencies.
Also if you force certain build options like 32bit target, no C compiler
optimizations (for debugging purposes), and other similar build time options,
those options are cached indefinitely until you issue a make distclean
command.
If after building Redis with a 32 bit target you need to rebuild it
with a 64 bit target, or the other way around, you need to perform a
make distclean in the root directory of the Redis distribution.
In case of build errors when trying to build a 32 bit binary of Redis, try
the following steps:
Selecting a non-default memory allocator when building Redis is done by setting
the MALLOC environment variable. Redis is compiled and linked against libc
malloc by default, with the exception of jemalloc being the default on Linux
systems. This default was picked because jemalloc has proven to have fewer
fragmentation problems than libc malloc.
To force compiling against libc malloc, use:
To compile against jemalloc on Mac OS X systems, use:
By default, Redis will build using the POSIX clock_gettime function as the
monotonic clock source. On most modern systems, the internal processor clock
can be used to improve performance. Cautions can be found here:
http://oliveryang.net/2015/09/pitfalls-of-TSC-usage/
To build with support for the processor's internal instruction clock, use:
Redis will build with a user-friendly colorized output by default.
If you want to see a more verbose output, use the following:
To run Redis with the default configuration, just type:
If you want to provide your redis.conf, you have to run it using an additional
parameter (the path of the configuration file):
It is possible to alter the Redis configuration by passing parameters directly
as options using the command line. Examples:
All the options in redis.conf are also supported as options using the command
line, with exactly the same name.
Please consult the TLS.md file for more information on
how to use Redis with TLS.
You can use redis-cli to play with Redis. Start a redis-server instance,
then in another terminal try the following:
You can find the list of all the available commands at https://redis.io/commands.
In order to install Redis binaries into /usr/local/bin, just use:
You can use make PREFIX=/some/other/directory install if you wish to use a
different destination.
make install will just install binaries in your system, but will not configure
init scripts and configuration files in the appropriate place. This is not
needed if you just want to play a bit with Redis, but if you are installing
it the proper way for a production system, we have a script that does this
for Ubuntu and Debian systems:
Note: install_server.sh will not work on Mac OSX; it is built for Linux only.
The script will ask you a few questions and will setup everything you need
to run Redis properly as a background daemon that will start again on
system reboots.
You'll be able to stop and start Redis using the script named
/etc/init.d/redis_<portnumber>, for instance /etc/init.d/redis_6379.
By contributing code to the Redis project in any form, including sending a pull request via GitHub,
a code fragment or patch via private email or public discussion groups, you agree to release your
code under the terms of the Redis Software Grant and Contributor License Agreement. Redis software
contains contributions to the original Redis core project, which are owned by their contributors and
licensed under the 3BSD license. Any copy of that license in this repository applies only to those
contributions. Redis releases all Redis project versions from 7.4.x and thereafter under the
RSALv2/SSPL dual-license as described in the LICENSE.txt file included in the Redis source distribution.
Please see the CONTRIBUTING.md file in this source distribution for more information. For
security bugs and vulnerabilities, please see SECURITY.md.
The purpose of a trademark is to identify the goods and services of a person or company without
causing confusion. As the registered owner of its name and logo, Redis accepts certain limited uses
of its trademarks but it has requirements that must be followed as described in its Trademark
Guidelines available at: https://redis.com/legal/trademark-guidelines/.
If you are reading this README you are likely in front of a Github page
or you just untarred the Redis distribution tar ball. In both the cases
you are basically one step away from the source code, so here we explain
the Redis source code layout, what is in each file as a general idea, the
most important functions and structures inside the Redis server and so forth.
We keep all the discussion at a high level without digging into the details
since this document would be huge otherwise and our code base changes
continuously, but a general idea should be a good starting point to
understand more. Moreover most of the code is heavily commented and easy
to follow.
The Redis root directory just contains this README, the Makefile which
calls the real Makefile inside the src directory and an example
configuration for Redis and Sentinel. You can find a few shell
scripts that are used in order to execute the Redis, Redis Cluster and
Redis Sentinel unit tests, which are implemented inside the tests
directory.
Inside the root are the following important directories:
There are a few more directories but they are not very important for our goals
here. We'll focus mostly on src, where the Redis implementation is contained,
exploring what there is inside each file. The order in which files are
exposed is the logical one to follow in order to disclose different layers
of complexity incrementally.
Note: lately Redis was refactored quite a bit. Function names and file
names have been changed, so you may find that this documentation reflects the
unstable branch more closely. For instance, in Redis 3.0 the server.c
and server.h files were named redis.c and redis.h. However the overall
structure is the same. Keep in mind that all the new developments and pull
requests should be performed against the unstable branch.
The simplest way to understand how a program works is to understand the
data structures it uses. So we'll start from the main header file of
Redis, which is server.h.
All the server configuration and in general all the shared state is
defined in a global structure called server, of type struct redisServer.
A few important fields in this structure are:
There are tons of other fields. Most fields are commented directly inside
the structure definition.
Another important Redis data structure is the one defining a client.
In the past it was called redisClient, now just client. The structure
has many fields, here we'll just show the main ones:
The client structure defines a connected client:
As you can see in the client structure above, arguments in a command
are described as robj structures. The following is the full robj
structure, which defines a Redis object:
Basically this structure can represent all the basic Redis data types like
strings, lists, sets, sorted sets and so forth. The interesting thing is that
it has a type field, so that it is possible to know what type a given
object has, and a refcount, so that the same object can be referenced
in multiple places without allocating it multiple times. Finally the ptr
field points to the actual representation of the object, which might vary
even for the same type, depending on the encoding used.
Redis objects are used extensively in the Redis internals, however in order
to avoid the overhead of indirect accesses, recently in many places
we just use plain dynamic strings not wrapped inside a Redis object.
This is the entry point of the Redis server, where the main() function
is defined. The following are the most important steps in order to startup
the Redis server.
There are two special functions called periodically by the event loop:
Inside server.c you can find code that handles other vital things of the Redis server:
This file is auto generated by utils/generate-command-code.py, the content is based on the JSON files in the src/commands folder.
These are meant to be the single source of truth about the Redis commands, and all the metadata about them.
These JSON files are not meant to be used by anyone directly, instead that metadata can be obtained via the COMMAND command.
This file defines all the I/O functions with clients, masters and replicas
(which in Redis are just special clients):
As you can guess from the names, these files implement the RDB and AOF
persistence for Redis. Redis uses a persistence model based on the fork()
system call in order to create a process with the same (shared) memory
content of the main Redis process. This secondary process dumps the content
of the memory on disk. This is used by rdb.c to create the snapshots
on disk and by aof.c in order to perform the AOF rewrite when the
append only file gets too big.
The implementation inside aof.c has additional functions in order to
implement an API that allows commands to append new commands into the AOF
file as clients execute them.
The call() function defined inside server.c is responsible for calling
the functions that in turn will write the commands into the AOF.
Certain Redis commands operate on specific data types; others are general.
Examples of generic commands are DEL and EXPIRE. They operate on keys
and not on their values specifically. All those generic commands are
defined inside db.c.
Moreover db.c implements an API in order to perform certain operations
on the Redis dataset without directly accessing the internal data structures.
The most important functions inside db.c which are used in many command
implementations are the following:
The rest of the file implements the generic commands exposed to the client.
The robj structure defining Redis objects was already described. Inside
object.c there are all the functions that operate with Redis objects at
a basic level, like functions to allocate new objects, handle the reference
counting and so forth. Notable functions inside this file:
This file also implements the OBJECT command.
This is one of the most complex files inside Redis, it is recommended to
approach it only after getting a bit familiar with the rest of the code base.
In this file there is the implementation of both the master and replica role
of Redis.
One of the most important functions inside this file is replicationFeedSlaves() that writes commands to the clients representing replica instances connected
to our master, so that the replicas can get the writes performed by the clients:
this way their data set will remain synchronized with the one in the master.
This file also implements both the SYNC and PSYNC commands that are
used in order to perform the first synchronization between masters and
replicas, or to continue the replication after a disconnection.
The script unit is composed of 3 units:
All the Redis commands are defined in the following way:
The command function is referenced by a JSON file, together with its metadata, see commands.c described above for details.
The command flags are documented in the comment above the struct redisCommand in server.h.
For other details, please refer to the COMMAND command. https://redis.io/commands/command/
After the command operates in some way, it returns a reply to the client,
usually using addReply() or a similar function defined inside networking.c.
There are tons of command implementations inside the Redis source code
that can serve as examples of actual commands implementations (e.g. pingCommand). Writing
a few toy commands can be a good exercise to get familiar with the code base.
There are also many other files not described here, but it is useless to
cover everything. We just want to help you with the first steps.
Eventually you'll find your way inside the Redis code base :-)
Enjoy!
Redis is an in-memory database that persists on disk. The data model is key-value, but many different kind of values are supported: Strings, Lists, Sets, Sorted Sets, Hashes, Streams, HyperLogLogs, Bitmaps.