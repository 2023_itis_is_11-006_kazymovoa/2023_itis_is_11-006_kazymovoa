GitHub - containers/podman: Podman: A tool for managing OCI containers and pods.
Skip to content
Toggle navigation
Sign in
Product
Actions
Automate any workflow
Packages
Host and manage packages
Security
Find and fix vulnerabilities
Codespaces
Instant dev environments
Copilot
Write better code with AI
Code review
Manage code changes
Issues
Plan and track work
Discussions
Collaborate outside of code
Explore
All features
Documentation
GitHub Skills
Blog
Solutions
For
Enterprise
Teams
Startups
Education
By Solution
CI/CD & Automation
DevOps
DevSecOps
Resources
Learning Pathways
White papers, Ebooks, Webinars
Customer Stories
Partners
Open Source
GitHub Sponsors
Fund open source developers
The ReadME Project
GitHub community articles
Repositories
Topics
Trending
Collections
Pricing
Search or jump to...
Search code, repositories, users, issues, pull requests...
Search
Clear
Search syntax tips
Provide feedback
We read every piece of feedback, and take your input very seriously.
Include my email address so I can be contacted
Cancel
Submit feedback
Saved searches
Use saved searches to filter your results more quickly
Name
Query
To see all available qualifiers, see our documentation.
Cancel
Create saved search
Sign in
Sign up
You signed in with another tab or window. Reload to refresh your session.
You signed out in another tab or window. Reload to refresh your session.
You switched accounts on another tab or window. Reload to refresh your session.
Dismiss alert
containers
/
podman
Public
Notifications
Fork
2.2k
Star
21.3k
Podman: A tool for managing OCI containers and pods.
podman.io
License
Apache-2.0 license
21.3k
stars
2.2k
forks
Branches
Tags
Activity
Star
Notifications
Code
Issues
557
Pull requests
59
Discussions
Actions
Projects
0
Wiki
Security
Insights
Additional navigation options
Code
Issues
Pull requests
Discussions
Actions
Projects
Wiki
Security
Insights
containers/podman
This commit does not belong to any branch on this repository, and may belong to a fork outside of the repository.
mainBranchesTagsGo to fileCodeFolders and filesNameNameLast commit messageLast commit dateLatest commit History22,148 Commits.fmf.fmf .github.github cmdcmd cnicni completionscompletions contribcontrib dependencies/analysesdependencies/analyses dockerdocker docsdocs hackhack libpodlibpod logologo pkgpkg plansplans rpmrpm testtest utilsutils vendorvendor versionversion .cirrus.yml.cirrus.yml .dockerignore.dockerignore .gitignore.gitignore .golangci.yml.golangci.yml .packit.sh.packit.sh .packit.yaml.packit.yaml .pre-commit-config.yaml.pre-commit-config.yaml .readthedocs.yaml.readthedocs.yaml CODE-OF-CONDUCT.mdCODE-OF-CONDUCT.md CONTRIBUTING.mdCONTRIBUTING.md DISTRO_PACKAGE.mdDISTRO_PACKAGE.md DOWNLOADS.mdDOWNLOADS.md ISSUE-EOL-POLICY.mdISSUE-EOL-POLICY.md LICENSELICENSE MakefileMakefile OWNERSOWNERS README.mdREADME.md RELEASE_NOTES.mdRELEASE_NOTES.md RELEASE_PROCESS.mdRELEASE_PROCESS.md SECURITY.mdSECURITY.md VagrantfileVagrantfile build_osx.mdbuild_osx.md build_windows.mdbuild_windows.md commands-demo.mdcommands-demo.md commands.mdcommands.md go.modgo.mod go.sumgo.sum install.mdinstall.md pyproject.tomlpyproject.toml rootless.mdrootless.md transfer.mdtransfer.md troubleshooting.mdtroubleshooting.md winmake.ps1winmake.ps1 View all filesRepository files navigationREADMECode of conductApache-2.0 licenseSecurity
Podman: A tool for managing OCI containers and pods
Podman (the POD MANager) is a tool for managing containers and images, volumes mounted into those containers, and pods made from groups of containers.
Podman runs containers on Linux, but can also be used on Mac and Windows systems using a Podman-managed virtual machine.
Podman is based on libpod, a library for container lifecycle management that is also contained in this repository. The libpod library provides APIs for managing containers, pods, container images, and volumes.
All releases are GPG signed. Public keys of members of the team approved to make releases are located here.
Continuous Integration:
GoDoc: Downloads
Overview and scope
At a high level, the scope of Podman and libpod is the following:
Support for multiple container image formats, including OCI and Docker images.
Full management of those images, including pulling from various sources (including trust and verification), creating (built via Containerfile or Dockerfile or committed from a container), and pushing to registries and other storage backends.
Full management of container lifecycle, including creation (both from an image and from an exploded root filesystem), running, checkpointing and restoring (via CRIU), and removal.
Full management of container networking, using Netavark.
Support for pods, groups of containers that share resources and are managed together.
Support for running containers and pods without root or other elevated privileges.
Resource isolation of containers and pods.
Support for a Docker-compatible CLI interface, which can both run containers locally and on remote systems.
No manager daemon, for improved security and lower resource utilization at idle.
Support for a REST API providing both a Docker-compatible interface and an improved interface exposing advanced Podman functionality.
Support for running on Windows and Mac via virtual machines run by podman machine.
Roadmap
Further improvements to podman machine to better support Podman Desktop and other developer usecases.
Support for conmon-rs, which will improve container logging.
Support for the BuildKit API.
Performance and stability improvements.
Reductions to the size of the Podman binary.
Communications
If you think you've identified a security issue in the project, please DO NOT report the issue publicly via the GitHub issue tracker, mailing list, or IRC.
Instead, send an email with as many details as possible to security@lists.podman.io. This is a private mailing list for the core maintainers.
For general questions and discussion, please use Podman's
channels.
For discussions around issues/bugs and features, you can use the GitHub
issues
and
PRs
tracking system.
There is also a mailing list at lists.podman.io.
You can subscribe by sending a message to podman-join@lists.podman.io with the subject subscribe.
Rootless
Podman can be easily run as a normal user, without requiring a setuid binary.
When run without root, Podman containers use user namespaces to set root in the container to the user running Podman.
Rootless Podman runs locked-down containers with no privileges that the user running the container does not have.
Some of these restrictions can be lifted (via --privileged, for example), but rootless containers will never have more privileges than the user that launched them.
If you run Podman as your user and mount in /etc/passwd from the host, you still won't be able to change it, since your user doesn't have permission to do so.
Almost all normal Podman functionality is available, though there are some shortcomings.
Any recent Podman release should be able to run rootless without any additional configuration, though your operating system may require some additional configuration detailed in the install guide.
A little configuration by an administrator is required before rootless Podman can be used, the necessary setup is documented here.
Podman Desktop
Podman Desktop provides a local development environment for Podman and Kubernetes on Linux, Windows, and Mac machines.
It is a full-featured desktop UI frontend for Podman which uses the podman machine backend on non-Linux operating systems to run containers.
It supports full container lifecycle management (building, pulling, and pushing images, creating and managing containers, creating and managing pods, and working with Kubernetes YAML).
The project develops on GitHub and contributors are welcome.
Out of scope
Specialized signing and pushing of images to various storage backends.
See Skopeo for those tasks.
Support for the Kubernetes CRI interface for container management.
The CRI-O daemon specializes in that.
OCI Projects Plans
Podman uses OCI projects and best of breed libraries for different aspects:
Runtime: We use the OCI runtime tools to generate OCI runtime configurations that can be used with any OCI-compliant runtime, like crun and runc.
Images: Image management uses the containers/image library.
Storage: Container and image storage is managed by containers/storage.
Networking: Networking support through use of Netavark and Aardvark. Rootless networking is handled via slirp4netns.
Builds: Builds are supported via Buildah.
Conmon: Conmon is a tool for monitoring OCI runtimes, used by both Podman and CRI-O.
Seccomp: A unified Seccomp policy for Podman, Buildah, and CRI-O.
Podman Information for Developers
For blogs, release announcements and more, please checkout the podman.io website!
Installation notes
Information on how to install Podman in your environment.
OCI Hooks Support
Information on how Podman configures OCI Hooks to run when launching a container.
Podman API
Documentation on the Podman REST API.
Podman Commands
A list of the Podman commands with links to their man pages and in many cases videos
showing the commands in use.
Podman Troubleshooting Guide
A list of common issues and solutions for Podman.
Podman Usage Transfer
Useful information for ops and dev transfer as it relates to infrastructure that utilizes Podman. This page
includes tables showing Docker commands and their Podman equivalent commands.
Tutorials
Tutorials on using Podman.
Remote Client
A brief how-to on using the Podman remote client.
Basic Setup and Use of Podman in a Rootless environment
A tutorial showing the setup and configuration necessary to run Rootless Podman.
Release Notes
Release notes for recent Podman versions.
Contributing
Information about contributing to this project.
Buildah and Podman relationship
Buildah and Podman are two complementary open-source projects that are
available on most Linux platforms and both projects reside at
GitHub.com with Buildah
here and Podman
here. Both, Buildah and Podman are
command line tools that work on Open Container Initiative (OCI) images and
containers. The two projects differentiate in their specialization.
Buildah specializes in building OCI images. Buildah's commands replicate all
of the commands that are found in a Dockerfile. This allows building images
with and without Dockerfiles while not requiring any root privileges.
Buildah’s ultimate goal is to provide a lower-level coreutils interface to
build images. The flexibility of building images without Dockerfiles allows
for the integration of other scripting languages into the build process.
Buildah follows a simple fork-exec model and does not run as a daemon
but it is based on a comprehensive API in golang, which can be vendored
into other tools.
Podman specializes in all of the commands and functions that help you to maintain and modify
OCI images, such as pulling and tagging. It also allows you to create, run, and maintain those containers
created from those images. For building container images via Dockerfiles, Podman uses Buildah's
golang API and can be installed independently from Buildah.
A major difference between Podman and Buildah is their concept of a container. Podman
allows users to create "traditional containers" where the intent of these containers is
to be long lived. While Buildah containers are really just created to allow content
to be added back to the container image. An easy way to think of it is the
buildah run command emulates the RUN command in a Dockerfile while the podman run
command emulates the docker run command in functionality. Because of this and their underlying
storage differences, you can not see Podman containers from within Buildah or vice versa.
In short, Buildah is an efficient way to create OCI images while Podman allows
you to manage and maintain those images and containers in a production environment using
familiar container cli commands. For more details, see the
Container Tools Guide.
Podman Hello
$ podman run quay.io/podman/hello
Trying to pull quay.io/podman/hello:latest...
Getting image source signatures
Copying blob a6b3126f3807 done
Copying config 25c667d086 done
Writing manifest to image destination
Storing signatures
!... Hello Podman World ...!
.--"--.
/ - - \
/ (O) (O) \
~~~| -=(,Y,)=- |
.---. /` \ |~~
~/ o o \~~~~.----. ~~
| =(X)= |~ / (O (O) \
~~~~~~~ ~| =(Y_)=- |
~~~~ ~~~| U |~~
Project: https://github.com/containers/podman
Website: https://podman.io
Documents: https://docs.podman.io
Twitter: @Podman_io
About
Podman: A tool for managing OCI containers and pods.
podman.io
Topics
linux
docker
kubernetes
containers
oci
Resources
Readme
License
Apache-2.0 license
Code of conduct
Code of conduct
Security policy
Security policy
Activity
Custom properties
Stars
21.3k
stars
Watchers
205
watching
Forks
2.2k
forks
Report repository
Releases
190
v5.0.0
Latest
Mar 19, 2024
+ 189 releases
Packages
0
No packages published Used by 134
+ 126
Contributors
620
+ 606 contributors
Languages
Go
81.8%
Shell
13.6%
Perl
2.2%
Python
1.1%
C
0.6%
Makefile
0.5%
Other
0.2%
Footer
© 2024 GitHub, Inc.
Footer navigation
Terms
Privacy
Security
Status
Docs
Contact
Manage cookies
Do not share my personal information
You can’t perform that action at this time.