GitHub - containerd/containerd: An open and reliable container runtime
Skip to content
Toggle navigation
Sign in
Product
Actions
Automate any workflow
Packages
Host and manage packages
Security
Find and fix vulnerabilities
Codespaces
Instant dev environments
Copilot
Write better code with AI
Code review
Manage code changes
Issues
Plan and track work
Discussions
Collaborate outside of code
Explore
All features
Documentation
GitHub Skills
Blog
Solutions
For
Enterprise
Teams
Startups
Education
By Solution
CI/CD & Automation
DevOps
DevSecOps
Resources
Learning Pathways
White papers, Ebooks, Webinars
Customer Stories
Partners
Open Source
GitHub Sponsors
Fund open source developers
The ReadME Project
GitHub community articles
Repositories
Topics
Trending
Collections
Pricing
Search or jump to...
Search code, repositories, users, issues, pull requests...
Search
Clear
Search syntax tips
Provide feedback
We read every piece of feedback, and take your input very seriously.
Include my email address so I can be contacted
Cancel
Submit feedback
Saved searches
Use saved searches to filter your results more quickly
Name
Query
To see all available qualifiers, see our documentation.
Cancel
Create saved search
Sign in
Sign up
You signed in with another tab or window. Reload to refresh your session.
You signed out in another tab or window. Reload to refresh your session.
You switched accounts on another tab or window. Reload to refresh your session.
Dismiss alert
containerd
/
containerd
Public
Notifications
Fork
3.2k
Star
16.1k
An open and reliable container runtime
containerd.io
License
Apache-2.0 license
16.1k
stars
3.2k
forks
Branches
Tags
Activity
Star
Notifications
Code
Issues
494
Pull requests
198
Discussions
Actions
Projects
1
Security
Insights
Additional navigation options
Code
Issues
Pull requests
Discussions
Actions
Projects
Security
Insights
containerd/containerd
This commit does not belong to any branch on this repository, and may belong to a fork outside of the repository.
mainBranchesTagsGo to fileCodeFolders and filesNameNameLast commit messageLast commit dateLatest commit History13,685 Commits.devcontainer.devcontainer .github.github apiapi clientclient clustercluster cmdcmd contribcontrib corecore defaultsdefaults docsdocs integrationintegration internalinternal pkgpkg pluginsplugins protobufprotobuf releasesreleases scriptscript testtest vendorvendor versionversion .gitattributes.gitattributes .gitignore.gitignore .golangci.yml.golangci.yml .lycheeignore.lycheeignore .mailmap.mailmap ADOPTERS.mdADOPTERS.md BUILDING.mdBUILDING.md CONTRIBUTING.mdCONTRIBUTING.md LICENSELICENSE MakefileMakefile Makefile.darwinMakefile.darwin Makefile.freebsdMakefile.freebsd Makefile.linuxMakefile.linux Makefile.windowsMakefile.windows NOTICENOTICE Protobuild.tomlProtobuild.toml README.mdREADME.md RELEASES.mdRELEASES.md ROADMAP.mdROADMAP.md SCOPE.mdSCOPE.md VagrantfileVagrantfile code-of-conduct.mdcode-of-conduct.md codecov.ymlcodecov.yml containerd.servicecontainerd.service go.modgo.mod go.sumgo.sum View all filesRepository files navigationREADMECode of conductApache-2.0 licenseSecurity
containerd is an industry-standard container runtime with an emphasis on simplicity, robustness, and portability. It is available as a daemon for Linux and Windows, which can manage the complete container lifecycle of its host system: image transfer and storage, container execution and supervision, low-level storage and network attachments, etc.
containerd is a member of CNCF with 'graduated' status.
containerd is designed to be embedded into a larger system, rather than being used directly by developers or end-users.
Announcements
Now Recruiting
We are a large inclusive OSS project that is welcoming help of any kind shape or form:
Documentation help is needed to make the product easier to consume and extend.
We need OSS community outreach/organizing help to get the word out; manage
and create messaging and educational content; and help with social media, community forums/groups, and google groups.
We are actively inviting new security advisors to join the team.
New subprojects are being created, core and non-core that could use additional development help.
Each of the containerd projects has a list of issues currently being worked on or that need help resolving.
If the issue has not already been assigned to someone or has not made recent progress, and you are interested, please inquire.
If you are interested in starting with a smaller/beginner-level issue, look for issues with an exp/beginner tag, for example containerd/containerd beginner issues.
Getting Started
See our documentation on containerd.io:
for ops and admins
namespaces
client options
To get started contributing to containerd, see CONTRIBUTING.
If you are interested in trying out containerd see our example at Getting Started.
Nightly builds
There are nightly builds available for download here.
Binaries are generated from main branch every night for Linux and Windows.
Please be aware: nightly builds might have critical bugs, it's not recommended for use in production and no support provided.
Kubernetes (k8s) CI Dashboard Group
The k8s CI dashboard group for containerd contains test results regarding
the health of kubernetes when run against main and a number of containerd release branches.
containerd-periodics
Runtime Requirements
Runtime requirements for containerd are very minimal. Most interactions with
the Linux and Windows container feature sets are handled via runc and/or
OS-specific libraries (e.g. hcsshim for Microsoft).
The current required version of runc is described in RUNC.md.
There are specific features
used by containerd core code and snapshotters that will require a minimum kernel
version on Linux. With the understood caveat of distro kernel versioning, a
reasonable starting point for Linux is a minimum 4.x kernel version.
The overlay filesystem snapshotter, used by default, uses features that were
finalized in the 4.x kernel series. If you choose to use btrfs, there may
be more flexibility in kernel version (minimum recommended is 3.18), but will
require the btrfs kernel module and btrfs tools to be installed on your Linux
distribution.
To use Linux checkpoint and restore features, you will need criu installed on
your system. See more details in Checkpoint and Restore.
Build requirements for developers are listed in BUILDING.
Supported Registries
Any registry which is compliant with the OCI Distribution Specification
is supported by containerd.
For configuring registries, see registry host configuration documentation
Features
Client
containerd offers a full client package to help you integrate containerd into your platform.
import (
"context"
containerd "github.com/containerd/containerd/v2/client"
"github.com/containerd/containerd/v2/pkg/cio"
"github.com/containerd/containerd/v2/pkg/namespaces"
)
func main() {
client, err := containerd.New("/run/containerd/containerd.sock")
defer client.Close()
}
Namespaces
Namespaces allow multiple consumers to use the same containerd without conflicting with each other. It has the benefit of sharing content while maintaining separation with containers and images.
To set a namespace for requests to the API:
context = context.Background()
// create a context for docker
docker = namespaces.WithNamespace(context, "docker")
containerd, err := client.NewContainer(docker, "id")
To set a default namespace on the client:
client, err := containerd.New(address, containerd.WithDefaultNamespace("docker"))
Distribution
// pull an image
image, err := client.Pull(context, "docker.io/library/redis:latest")
// push an image
err := client.Push(context, "docker.io/library/redis:latest", image.Target())
Containers
In containerd, a container is a metadata object. Resources such as an OCI runtime specification, image, root filesystem, and other metadata can be attached to a container.
redis, err := client.NewContainer(context, "redis-master")
defer redis.Delete(context)
OCI Runtime Specification
containerd fully supports the OCI runtime specification for running containers. We have built-in functions to help you generate runtime specifications based on images as well as custom parameters.
You can specify options when creating a container about how to modify the specification.
redis, err := client.NewContainer(context, "redis-master", containerd.WithNewSpec(oci.WithImageConfig(image)))
Root Filesystems
containerd allows you to use overlay or snapshot filesystems with your containers. It comes with built-in support for overlayfs and btrfs.
// pull an image and unpack it into the configured snapshotter
image, err := client.Pull(context, "docker.io/library/redis:latest", containerd.WithPullUnpack)
// allocate a new RW root filesystem for a container based on the image
redis, err := client.NewContainer(context, "redis-master",
containerd.WithNewSnapshot("redis-rootfs", image),
containerd.WithNewSpec(oci.WithImageConfig(image)),
)
// use a readonly filesystem with multiple containers
for i := 0; i < 10; i++ {
id := fmt.Sprintf("id-%s", i)
container, err := client.NewContainer(ctx, id,
containerd.WithNewSnapshotView(id, image),
containerd.WithNewSpec(oci.WithImageConfig(image)),
)
}
Tasks
Taking a container object and turning it into a runnable process on a system is done by creating a new Task from the container. A task represents the runnable object within containerd.
// create a new task
task, err := redis.NewTask(context, cio.NewCreator(cio.WithStdio))
defer task.Delete(context)
// the task is now running and has a pid that can be used to setup networking
// or other runtime settings outside of containerd
pid := task.Pid()
// start the redis-server process inside the container
err := task.Start(context)
// wait for the task to exit and get the exit status
status, err := task.Wait(context)
Checkpoint and Restore
If you have criu installed on your machine you can checkpoint and restore containers and their tasks. This allows you to clone and/or live migrate containers to other machines.
// checkpoint the task then push it to a registry
checkpoint, err := task.Checkpoint(context)
err := client.Push(context, "myregistry/checkpoints/redis:master", checkpoint)
// on a new machine pull the checkpoint and restore the redis container
checkpoint, err := client.Pull(context, "myregistry/checkpoints/redis:master")
redis, err = client.NewContainer(context, "redis-master", containerd.WithNewSnapshot("redis-rootfs", checkpoint))
defer container.Delete(context)
task, err = redis.NewTask(context, cio.NewCreator(cio.WithStdio), containerd.WithTaskCheckpoint(checkpoint))
defer task.Delete(context)
err := task.Start(context)
Snapshot Plugins
In addition to the built-in Snapshot plugins in containerd, additional external
plugins can be configured using GRPC. An external plugin is made available using
the configured name and appears as a plugin alongside the built-in ones.
To add an external snapshot plugin, add the plugin to containerd's config file
(by default at /etc/containerd/config.toml). The string following
proxy_plugin. will be used as the name of the snapshotter and the address
should refer to a socket with a GRPC listener serving containerd's Snapshot
GRPC API. Remember to restart containerd for any configuration changes to take
effect.
[proxy_plugins]
[proxy_plugins.customsnapshot]
type = "snapshot"
address = "/var/run/mysnapshotter.sock"
See PLUGINS.md for how to create plugins
Releases and API Stability
Please see RELEASES.md for details on versioning and stability
of containerd components.
Downloadable 64-bit Intel/AMD binaries of all official releases are available on
our releases page.
For other architectures and distribution support, you will find that many
Linux distributions package their own containerd and provide it across several
architectures, such as Canonical's Ubuntu packaging.
Enabling command auto-completion
Starting with containerd 1.4, the urfave client feature for auto-creation of bash and zsh
autocompletion data is enabled. To use the autocomplete feature in a bash shell for example, source
the autocomplete/ctr file in your .bashrc, or manually like:
$ source ./contrib/autocomplete/ctr
Distribution of ctr autocomplete for bash and zsh
For bash, copy the contrib/autocomplete/ctr script into
/etc/bash_completion.d/ and rename it to ctr. The zsh_autocomplete
file is also available and can be used similarly for zsh users.
Provide documentation to users to source this file into their shell if
you don't place the autocomplete file in a location where it is automatically
loaded for the user's shell environment.
CRI
cri is a containerd plugin implementation of the Kubernetes container runtime interface (CRI). With it, you are able to use containerd as the container runtime for a Kubernetes cluster.
CRI Status
cri is a native plugin of containerd. Since containerd 1.1, the cri plugin is built into the release binaries and enabled by default.
The cri plugin has reached GA status, representing that it is:
Feature complete
Works with Kubernetes 1.10 and above
Passes all CRI validation tests.
Passes all node e2e tests.
Passes all e2e tests.
See results on the containerd k8s test dashboard
Validating Your cri Setup
A Kubernetes incubator project, cri-tools, includes programs for exercising CRI implementations. More importantly, cri-tools includes the program critest which is used for running CRI Validation Testing.
CRI Guides
Installing with Ansible and Kubeadm
For Non-Ansible Users, Preforming a Custom Installation Using the Release Tarball and Kubeadm
CRI Plugin Testing Guide
Debugging Pods, Containers, and Images with crictl
Configuring cri Plugins
Configuring containerd
Communication
For async communication and long-running discussions please use issues and pull requests on the GitHub repo.
This will be the best place to discuss design and implementation.
For sync communication catch us in the #containerd and #containerd-dev Slack channels on Cloud Native Computing Foundation's (CNCF) Slack - cloud-native.slack.com. Everyone is welcome to join and chat. Get Invite to CNCF Slack.
Security audit
Security audits for the containerd project are hosted on our website. Please see the security page at containerd.io for more information.
Reporting security issues
Please follow the instructions at containerd/project
Licenses
The containerd codebase is released under the Apache 2.0 license.
The README.md file and files in the "docs" folder are licensed under the
Creative Commons Attribution 4.0 International License. You may obtain a
copy of the license, titled CC-BY-4.0, at http://creativecommons.org/licenses/by/4.0/.
Project details
containerd is the primary open source project within the broader containerd GitHub organization.
However, all projects within the repo have common maintainership, governance, and contributing
guidelines which are stored in a project repository commonly for all containerd projects.
Please find all these core project documents, including the:
Project governance,
Maintainers,
and Contributing guidelines
information in our containerd/project repository.
Adoption
Interested to see who is using containerd? Are you using containerd in a project?
Please add yourself via pull request to our ADOPTERS.md file.
About
An open and reliable container runtime
containerd.io
Topics
docker
kubernetes
containers
cncf
oci
cri
hacktoberfest
containerd
Resources
Readme
License
Apache-2.0 license
Code of conduct
Code of conduct
Security policy
Security policy
Activity
Custom properties
Stars
16.1k
stars
Watchers
329
watching
Forks
3.2k
forks
Report repository
Releases
200
containerd 1.7.14
Latest
Mar 11, 2024
+ 199 releases
Packages
8
+ 5 packages
Used by 22.9k
+ 22,908
Contributors
576
+ 562 contributors
Languages
Go
97.5%
Shell
1.7%
Other
0.8%
Footer
© 2024 GitHub, Inc.
Footer navigation
Terms
Privacy
Security
Status
Docs
Contact
Manage cookies
Do not share my personal information
You can’t perform that action at this time.