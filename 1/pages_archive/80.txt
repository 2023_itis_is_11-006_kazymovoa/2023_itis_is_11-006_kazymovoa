There are many ways to start Gazebo, open world models and spawn robot models into the simulated environment. In this tutorial we cover the ROS-way of doing things: using rosrun and roslaunch. This includes storing your URDF files in ROS packages and keeping your various resource paths relative to your ROS workspace.
The roslaunch tool is the standard method for starting ROS nodes and bringing up robots in ROS. To start an empty Gazebo world similar to the rosrun command in the previous tutorial, simply run
You can append the following arguments to the launch files to change the behavior of Gazebo:
paused
Start Gazebo in a paused state (default false)
use_sim_time
Tells ROS nodes asking for time to get the Gazebo-published simulation time, published over the ROS topic /clock (default true)
gui
Launch the user interface window of Gazebo (default true)
headless (deprecated)
recording (previously called headless)
Enable gazebo state log recording
debug
Start gzserver (Gazebo Server) in debug mode using gdb (default false)
verbose
Run gzserver and gzclient with --verbose, printing errors and warnings to the terminal (default false)
server_required
Terminate launch script when gzserver (Gazebo Server) exits (default false)
gui_required
Terminate launch script when gzclient (user interface window) exits (default false)
Normally the default values for these arguments are all you need, but just as an example:
Other demo worlds are already included in the gazebo_ros package, including:
Notice in mud_world.launch a simple jointed mechanism is launched. The launch file for mud_world.launch contains the following:
In this launch file we inherit most of the necessary functionality from
empty_world.launch. The only parameter we need to change is the world_name
parameter, substituting the empty.world world file with the mud.world file.
The other arguments are simply set to their default values.
Continuing with our examination of the mud_world.launch file, we will now look at the contents of the mud.world file. The first several components of the mud world is shown below:
See the section below to view this full world file on your computer.
In this world file snippet you can see that three models are referenced. The three models are searched for within your local Gazebo Model Database. If not found there, they are automatically pulled from Gazebo's online database.
You can learn more about world files in the Build A World tutorial.
World files are found within the /worlds directory of your Gazebo resource path. The location of this path depends on how you installed Gazebo and the type of system your are on. To find the location of your Gazebo resources, use the following command:
An typical path might be something like /usr/local/share/gazebo-1.9. Add /worlds to the end of the path and you should have the directory containing the world files Gazebo uses, including the mud.world file.
Before continuing on how to spawn robots into Gazebo,
we will first go over file hierarchy standards for using
ROS with Gazebo so that we can make later assumptions.
For now, we will assume your catkin workspace is named catkin_ws,
though you can name this to whatever you want.
Thus, your catkin workspace might be located on your computer at something like:
Everything concerning your robot's model and description is located,
as per ROS standards, in a package named /MYROBOT_description
and all the world files and launch files used with Gazebo is located in a ROS package named
/MYROBOT_gazebo. Replace 'MYROBOT' with the name of your bot in lower case letters.
With these two packages, your hierarchy should be as follows:
Remember that the command catkin_create_pkg is used for creating new packages,
though this can also easily be adapted for rosbuild if you must.
Most of these folders and files should be self explanatory.
The next section will walk you through making some of this setup for use with a custom world file.
You can create custom .world files within your own ROS packages that are specific to your robots and packages. In this mini tutorial we'll make an empty world with a ground, a sun, and a gas station. The following is our recommended convention. Be sure to replace MYROBOT with the name of your bot, or if you don't have a robot to test with just replace it with something like 'test':
You should see the following world model (zoom out with the scroll wheel on your mouse):
For the rest of the tutorial, your MYROBOT_gazebo empty world needs to be running.
You can insert additional models into your robot's world file and use the File->Save As command to export your edited world back into your ROS package.
There are two ways to launch your URDF-based robot into Gazebo using roslaunch:
ROS Service Call Spawn Method
The first method keeps your robot's ROS packages more portable between computers and repository check outs. It allows you to keep your robot's location relative to a ROS package path, but also requires you to make a ROS service call using a small (python) script.
Model Database Method
The second method allows you to include your robot within the .world file, which seems cleaner and more convenient but requires you to add your robot to the Gazebo model database by setting an environment variable.
We will go over both methods. Overall our recommended method is using the '''ROS Service Call Spawn Method'''
This method uses a small python script called spawn_model to make
a service call request to the gazebo_ros ROS node
(named simply "gazebo" in the rostopic namespace) to add a custom URDF into Gazebo.
The spawn_model script is located within the gazebo_ros package.
You can use this script in the following way:
To see all of the available arguments for spawn_model including namespaces, trimesh properties, joint positions and RPY orientation run:
If you do not yet have a URDF to test, as an example you can download the baxter_description package from Rethink Robotics's baxter_common repo. Put this package into your catkin workspace by running:
You should now have a URDF file named baxter.urdf located in a within baxter_description/urdf/, and you can run:
You should then see something similar to:
To integrate this directly into a ROS launch file, reopen the file MYROBOT_gazebo/launch/YOUROBOT.launch and add the following before the </launch> tag:
Launching this file, you should see the same results as when using rosrun.
If your URDF is not in XML format but rather in XACRO format, you can make a similar modification to your launch file. You can run this PR2 example by installing this package:
ROS Jade:
sudo apt-get install ros-jade-pr2-common
Then adding this to your launch file created previously in this tutorial:
Launching this file, you should see the PR2 in the gas station as pictured:
Note: at this writing there are still a lot of errors and warnings from the console output that need to be fixed from the PR2's URDF due to Gazebo API changes.
The second method of spawning robots into Gazebo allows you to include your robot within the .world file, which seems cleaner and more convenient but also requires you to add your robot to the Gazebo model database by setting an environment variable. This environment variable is required because of the separation of ROS dependencies from Gazebo; URDF package paths cannot be used directly inside .world files because Gazebo does not have a notion of ROS packages.
To accomplish this method, you must make a new model database that contains just your single robot. This isn't the cleanest way to load your URDF into Gazebo but accomplishes the goal of not having to keep two copies of your robot URDF on your computer. If the following instructions are confusing, refer back to the Gazebo Model Database documentation to understand why these steps are required.
We will assume your ROS workspace file hierarchy is setup as described in the above sections. The only difference is that now a model.config file is added to your MYROBOT_description package like so:
This hierarchy is specially adapted for use as a Gazebo model database by means of the following folders/files:
Each model must have a model.config file in the model's root directory that contains meta information about the model. Basically copy this into a model.config file, replacing MYROBOT.urdf with your file name:
Unlike for SDFs, no version is required for the tag when it is used for URDFs. See the Gazebo Model Database documentation for more info.
Finally, you need to add an environment variable to your .bashrc file that tells Gazebo where to look for model databases.
Using the editor of your choice edit "~/.bashrc".
Check if you already have a GAZEBO_MODEL_PATH defined.
If you already have one, append to it using a semi-colon, otherwise add the new export.
Assuming your Catkin workspace is in ~/catkin_ws/ Your path should look something like:
Now test to see if your new Gazebo Model Database is properly configured by launching Gazebo:
And clicking the "Insert" tab on the left. You will probably see several different drop down lists that represent different model databases available on your system, including the online database. Find the database corresponding to your robot, open the sub menu, click on the name of your robot and then choose a location within Gazebo to place the robot, using your mouse.
The advantage of the model database method is that now you can include your robot directly within your world files, without using a ROS package path. We'll use the same setup from the section "Creating a world file" but modify the world file:
The disadvantage of this method is that your packaged MYROBOT_description and MYROBOT_gazebo
are not as easily portable between computers - you first have to set the GAZEBO_MODEL_PATH
on any new system before being able to use these ROS packages.
The useful info would be the format for exporting model paths from a package.xml:
The '${prefix}` is something that new users might not immediately know about either, and necessary here.
Also would be useful to have some info on how to debug these paths from the ROS side, e.g. that you can use rospack plugins --attrib="gazebo_media_path" gazebo_ros
To check the media path that will be picked up by gazebo.
Now that you know how to create roslaunch files that open Gazebo, world files and URDF models, you are now ready to create your own Gazebo-ready URDF model in the tutorial Using A URDF In Gazebo
©2014 Open Source Robotics Foundation
Gazebo is open-source licensed under Apache 2.0