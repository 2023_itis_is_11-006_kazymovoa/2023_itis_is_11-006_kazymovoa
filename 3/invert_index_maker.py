import json
from collections import defaultdict
from tqdm import tqdm
import dto


def doc_id2word_list(n: int):
    doc_id2word_dict = {}
    for i in tqdm(range(1, n + 1)):
        with open(f"../2/cleaned_docs/{i}.txt", "r", encoding="utf-8") as file:
            doc_id2word_dict[i] = file.read().split()
    return doc_id2word_dict


def doc_id2word_set(n: int):
    doc_id2word_dict = {}
    for i in tqdm(range(1, n + 1)):
        with open(f"../2/cleaned_docs/{i}.txt", "r", encoding="utf-8") as file:
            doc_id2word_dict[i] = set(file.read().split())
    return doc_id2word_dict


def word_list2doc_id(doc_id2word_dict):
    inverted_idx = defaultdict(set)
    for idx, words in doc_id2word_dict.items():
        for word in words:
            inverted_idx[word].add(idx)
    return inverted_idx


def main():
    doc_id2word_dict = doc_id2word_set(100)
    inverted_idx = word_list2doc_id(doc_id2word_dict)
    inverted_idx = dto.toJsonSerializable(inverted_idx)
    with open("inverted_index.txt", "w", encoding="utf-8") as file:
        for k in sorted(inverted_idx.keys()):
            file.write(f"{k}\t{repr(inverted_idx[k])}\n")


if __name__ == "__main__":
    main()
