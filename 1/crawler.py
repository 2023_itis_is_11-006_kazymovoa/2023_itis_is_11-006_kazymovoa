import argparse
import random
import time

import requests
import re

from bs4 import BeautifulSoup
from urllib.parse import urlparse


def get_domain_from_link(link):
    parsed_url = urlparse(link)
    if parsed_url.netloc:
        if parsed_url.scheme:
            return parsed_url.scheme + "://" + parsed_url.netloc
        return parsed_url.netloc
    else:
        return link.split("/")[0]


def clean_text(text: str):
    text = re.sub(r'(?<=\s)\s+', '', text.strip().strip("\n"))
    return text


def download_page(url):
    try:
        response = requests.get(url)
        if response.status_code == 200:
            return response.text, None
        return None, Exception("Error downloading")
    except Exception as e:
        return None, e


def extract_links_and_text(html, domain):
    soup = BeautifulSoup(html, 'html.parser')
    links = []
    for link in soup.find_all('a'):
        href = link.get('href')
        if href:
            if not href.startswith('http'):
                href = domain + href
            links.append(href)
    # Remove script and style tags
    for script in soup(["script", "style"]):
        script.extract()
    # Get text
    text = soup.get_text()
    text = re.sub(r"<p/?>", "\n", text)
    text = re.sub(r"<[^>]*>", " ", text)
    text = clean_text(text)
    return links, text


def save_page(text, filename):
    with open(filename, 'w', encoding="utf-8") as file:
        file.write(text)


def main():
    parser = argparse.ArgumentParser(description='Download web pages and save them as text files.')
    parser.add_argument('urls', nargs='+', help='List of URLs to download pages from')
    args = parser.parse_args()

    downloaded_pages = 0
    urls: list[str] = list(args.urls)
    i = 0
    with open('index.txt', 'w', encoding='utf-8') as _:
        pass
    seen_urls: set[str] = set()
    while i < len(urls) and downloaded_pages < 100:
        if urls[i] in seen_urls:
            i += 1
            continue
        seen_urls.add(urls[i])
        html_content, err = download_page(urls[i])
        if err is not None:
            print(err)
            i += 1
            continue

        domain = get_domain_from_link(urls[i])
        if not html_content:
            print(f"Не удалось загрузить страницу: {urls[i]}")
            i += 1
            continue

        links, text = extract_links_and_text(html_content, domain)
        # print(text)
        urls.extend(links)
        words = re.findall(r'\w+', text)
        if len(words) < 1000 or len(words) > 20000:
            print(f"Ссылка {urls[i]} содержит менее 1000 слов (или более 20000) и не будет сохранена.")
            i += 1
            continue

        filename = f"pages/{downloaded_pages + 1}.txt"
        save_page(text, filename)
        with open('index.txt', 'a', encoding='utf-8') as index_file:
            index_file.write(f"{downloaded_pages + 1}: {urls[i]}\n")
        downloaded_pages += 1
        print(f"Успешно загружена страница {downloaded_pages}, размером в {len(words)} слов.")
        i += 1
        # time.sleep(random.uniform(0.2, 1))


if __name__ == "__main__":
    main()
