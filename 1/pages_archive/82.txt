The ROS Wiki is for ROS 1. Are you using ROS 2 (Humble, Iron, or Rolling)? Check out the ROS 2 Project DocumentationPackage specific documentation can be found on index.ros.org
Please ask about problems and questions regarding this tutorial on answers.ros.org. Don't forget to include in your question the link to this page, the versions of your OS & ROS, and also add appropriate tags.
Getting started with ROS and Docker Compose
Description: This tutorial walks you through using Docker Compose with ROS containers.
Keywords: ROS, Docker, Compose
Tutorial Level: BEGINNER
Compose is a tool for defining and running complex applications with Docker. With Compose, you define a multi-container application in a single file, then spin your application up in a single command which does everything that needs to be done to get it running. So basically, this will help shorten what you'd need to type into the Docker CLI to achieve the equivalent (concept is very similar to what roslaunch is for ROS). Before we get ahead of ourselves, its highly recommended to look over the Official Docker Compose Documentation to get familiar with what it's good for and what it can do, then come on back here to see how we can apply this tool to ROS specifically. Contents
Installing Compose
Using Compose
Making ROS Compose files
Running ROS Compose files
Stopping ROS Compose files
Using GUI's with compose Installing Compose
See https://docs.docker.com/compose/install/ Using Compose
Making ROS Compose files
Example compose file version: '2'
networks:
ros:
driver: bridge
services:
ros-master:
image: ros:kinetic-ros-core
command: stdbuf -o L roscore
networks:
- ros
restart: always
talker:
image: ros:kinetic-ros-core
depends_on:
- ros-master
environment:
- "ROS_MASTER_URI=http://ros-master:11311"
command: stdbuf -o L rostopic pub /chatter std_msgs/String "hello" -r 1
networks:
- ros
restart: always
listener:
image: ros:kinetic-ros-core
depends_on:
- ros-master
environment:
- "ROS_MASTER_URI=http://ros-master:11311"
command: stdbuf -o L rostopic echo /chatter
networks:
- ros
restart: always
Running ROS Compose files
docker-compose upor to run in background docker-compose up -d
Stopping ROS Compose files
docker-compose stop
Using GUI's with compose
If you want to have some sort of GUI starting with your compose you will need to add a container as described in Using GUI's with docker. Extending the example above, one way is to share your local X11-server with the container, log in as yourself and use your GUI as you like: viz:
image: osrf/ros:kinetic-desktop-full
container_name: ros_visualizer
depends_on:
- ros-master
networks:
- ros
environment:
- "ROS_MASTER_URI=http://ros-master:11311"
- "DISPLAY"
- "QT_X11_NO_MITSHM=1" #fix some QT bugs
#share your user to the container in order to access your x11
user: 1000:1000 #adapt as needed!
volumes: #share your x11 socket and permissions to the container
- /tmp/.X11-unix:/tmp/.X11-unix:rw
- /etc/group:/etc/group:ro
- /etc/passwd:/etc/passwd:ro
- /etc/shadow:/etc/shadow:ro
- /etc/sudoers:/etc/sudoers:ro
- /etc/sudoers.d:/etc/sudoers.d:ro
- /home/<your_user>:/home/<your_user>:rw #share your home with write permissions
command: rqtDefining your users UID and GID (as well as other variables) can be done with an .env-file (see Official env-file documentation): UGID=1000:1000and changing above example from user: 1000:1000to user: ${UGID}
Compose is a tool for defining and running complex applications with Docker. With Compose, you define a multi-container application in a single file, then spin your application up in a single command which does everything that needs to be done to get it running. So basically, this will help shorten what you'd need to type into the Docker CLI to achieve the equivalent (concept is very similar to what roslaunch is for ROS). Before we get ahead of ourselves, its highly recommended to look over the Official Docker Compose Documentation to get familiar with what it's good for and what it can do, then come on back here to see how we can apply this tool to ROS specifically. Contents
Installing Compose
Using Compose
Making ROS Compose files
Running ROS Compose files
Stopping ROS Compose files
Using GUI's with compose Installing Compose
See https://docs.docker.com/compose/install/ Using Compose
Making ROS Compose files
Example compose file version: '2'
networks:
ros:
driver: bridge
services:
ros-master:
image: ros:kinetic-ros-core
command: stdbuf -o L roscore
networks:
- ros
restart: always
talker:
image: ros:kinetic-ros-core
depends_on:
- ros-master
environment:
- "ROS_MASTER_URI=http://ros-master:11311"
command: stdbuf -o L rostopic pub /chatter std_msgs/String "hello" -r 1
networks:
- ros
restart: always
listener:
image: ros:kinetic-ros-core
depends_on:
- ros-master
environment:
- "ROS_MASTER_URI=http://ros-master:11311"
command: stdbuf -o L rostopic echo /chatter
networks:
- ros
restart: always
Running ROS Compose files
docker-compose upor to run in background docker-compose up -d
Stopping ROS Compose files
docker-compose stop
Using GUI's with compose
If you want to have some sort of GUI starting with your compose you will need to add a container as described in Using GUI's with docker. Extending the example above, one way is to share your local X11-server with the container, log in as yourself and use your GUI as you like: viz:
image: osrf/ros:kinetic-desktop-full
container_name: ros_visualizer
depends_on:
- ros-master
networks:
- ros
environment:
- "ROS_MASTER_URI=http://ros-master:11311"
- "DISPLAY"
- "QT_X11_NO_MITSHM=1" #fix some QT bugs
#share your user to the container in order to access your x11
user: 1000:1000 #adapt as needed!
volumes: #share your x11 socket and permissions to the container
- /tmp/.X11-unix:/tmp/.X11-unix:rw
- /etc/group:/etc/group:ro
- /etc/passwd:/etc/passwd:ro
- /etc/shadow:/etc/shadow:ro
- /etc/sudoers:/etc/sudoers:ro
- /etc/sudoers.d:/etc/sudoers.d:ro
- /home/<your_user>:/home/<your_user>:rw #share your home with write permissions
command: rqtDefining your users UID and GID (as well as other variables) can be done with an .env-file (see Official env-file documentation): UGID=1000:1000and changing above example from user: 1000:1000to user: ${UGID}
Compose is a tool for defining and running complex applications with Docker. With Compose, you define a multi-container application in a single file, then spin your application up in a single command which does everything that needs to be done to get it running. So basically, this will help shorten what you'd need to type into the Docker CLI to achieve the equivalent (concept is very similar to what roslaunch is for ROS). Before we get ahead of ourselves, its highly recommended to look over the Official Docker Compose Documentation to get familiar with what it's good for and what it can do, then come on back here to see how we can apply this tool to ROS specifically. Contents
Installing Compose
Using Compose
Making ROS Compose files
Running ROS Compose files
Stopping ROS Compose files
Using GUI's with compose Installing Compose
See https://docs.docker.com/compose/install/ Using Compose
Making ROS Compose files
Example compose file version: '2'
networks:
ros:
driver: bridge
services:
ros-master:
image: ros:kinetic-ros-core
command: stdbuf -o L roscore
networks:
- ros
restart: always
talker:
image: ros:kinetic-ros-core
depends_on:
- ros-master
environment:
- "ROS_MASTER_URI=http://ros-master:11311"
command: stdbuf -o L rostopic pub /chatter std_msgs/String "hello" -r 1
networks:
- ros
restart: always
listener:
image: ros:kinetic-ros-core
depends_on:
- ros-master
environment:
- "ROS_MASTER_URI=http://ros-master:11311"
command: stdbuf -o L rostopic echo /chatter
networks:
- ros
restart: always
Running ROS Compose files
docker-compose upor to run in background docker-compose up -d
Stopping ROS Compose files
docker-compose stop
Using GUI's with compose
If you want to have some sort of GUI starting with your compose you will need to add a container as described in Using GUI's with docker. Extending the example above, one way is to share your local X11-server with the container, log in as yourself and use your GUI as you like: viz:
image: osrf/ros:kinetic-desktop-full
container_name: ros_visualizer
depends_on:
- ros-master
networks:
- ros
environment:
- "ROS_MASTER_URI=http://ros-master:11311"
- "DISPLAY"
- "QT_X11_NO_MITSHM=1" #fix some QT bugs
#share your user to the container in order to access your x11
user: 1000:1000 #adapt as needed!
volumes: #share your x11 socket and permissions to the container
- /tmp/.X11-unix:/tmp/.X11-unix:rw
- /etc/group:/etc/group:ro
- /etc/passwd:/etc/passwd:ro
- /etc/shadow:/etc/shadow:ro
- /etc/sudoers:/etc/sudoers:ro
- /etc/sudoers.d:/etc/sudoers.d:ro
- /home/<your_user>:/home/<your_user>:rw #share your home with write permissions
command: rqtDefining your users UID and GID (as well as other variables) can be done with an .env-file (see Official env-file documentation): UGID=1000:1000and changing above example from user: 1000:1000to user: ${UGID}
Contents
Installing Compose
Using Compose
Making ROS Compose files
Running ROS Compose files
Stopping ROS Compose files
Using GUI's with compose Installing Compose
See https://docs.docker.com/compose/install/ Using Compose
Making ROS Compose files
Example compose file version: '2'
networks:
ros:
driver: bridge
services:
ros-master:
image: ros:kinetic-ros-core
command: stdbuf -o L roscore
networks:
- ros
restart: always
talker:
image: ros:kinetic-ros-core
depends_on:
- ros-master
environment:
- "ROS_MASTER_URI=http://ros-master:11311"
command: stdbuf -o L rostopic pub /chatter std_msgs/String "hello" -r 1
networks:
- ros
restart: always
listener:
image: ros:kinetic-ros-core
depends_on:
- ros-master
environment:
- "ROS_MASTER_URI=http://ros-master:11311"
command: stdbuf -o L rostopic echo /chatter
networks:
- ros
restart: always
Running ROS Compose files
docker-compose upor to run in background docker-compose up -d
Stopping ROS Compose files
docker-compose stop
Using GUI's with compose
If you want to have some sort of GUI starting with your compose you will need to add a container as described in Using GUI's with docker. Extending the example above, one way is to share your local X11-server with the container, log in as yourself and use your GUI as you like: viz:
image: osrf/ros:kinetic-desktop-full
container_name: ros_visualizer
depends_on:
- ros-master
networks:
- ros
environment:
- "ROS_MASTER_URI=http://ros-master:11311"
- "DISPLAY"
- "QT_X11_NO_MITSHM=1" #fix some QT bugs
#share your user to the container in order to access your x11
user: 1000:1000 #adapt as needed!
volumes: #share your x11 socket and permissions to the container
- /tmp/.X11-unix:/tmp/.X11-unix:rw
- /etc/group:/etc/group:ro
- /etc/passwd:/etc/passwd:ro
- /etc/shadow:/etc/shadow:ro
- /etc/sudoers:/etc/sudoers:ro
- /etc/sudoers.d:/etc/sudoers.d:ro
- /home/<your_user>:/home/<your_user>:rw #share your home with write permissions
command: rqtDefining your users UID and GID (as well as other variables) can be done with an .env-file (see Official env-file documentation): UGID=1000:1000and changing above example from user: 1000:1000to user: ${UGID}
Contents
Installing Compose
Using Compose
Making ROS Compose files
Running ROS Compose files
Stopping ROS Compose files
Using GUI's with compose
Installing Compose
See https://docs.docker.com/compose/install/ Using Compose
Making ROS Compose files
Example compose file version: '2'
networks:
ros:
driver: bridge
services:
ros-master:
image: ros:kinetic-ros-core
command: stdbuf -o L roscore
networks:
- ros
restart: always
talker:
image: ros:kinetic-ros-core
depends_on:
- ros-master
environment:
- "ROS_MASTER_URI=http://ros-master:11311"
command: stdbuf -o L rostopic pub /chatter std_msgs/String "hello" -r 1
networks:
- ros
restart: always
listener:
image: ros:kinetic-ros-core
depends_on:
- ros-master
environment:
- "ROS_MASTER_URI=http://ros-master:11311"
command: stdbuf -o L rostopic echo /chatter
networks:
- ros
restart: always
Running ROS Compose files
docker-compose upor to run in background docker-compose up -d
Stopping ROS Compose files
docker-compose stop
Using GUI's with compose
If you want to have some sort of GUI starting with your compose you will need to add a container as described in Using GUI's with docker. Extending the example above, one way is to share your local X11-server with the container, log in as yourself and use your GUI as you like: viz:
image: osrf/ros:kinetic-desktop-full
container_name: ros_visualizer
depends_on:
- ros-master
networks:
- ros
environment:
- "ROS_MASTER_URI=http://ros-master:11311"
- "DISPLAY"
- "QT_X11_NO_MITSHM=1" #fix some QT bugs
#share your user to the container in order to access your x11
user: 1000:1000 #adapt as needed!
volumes: #share your x11 socket and permissions to the container
- /tmp/.X11-unix:/tmp/.X11-unix:rw
- /etc/group:/etc/group:ro
- /etc/passwd:/etc/passwd:ro
- /etc/shadow:/etc/shadow:ro
- /etc/sudoers:/etc/sudoers:ro
- /etc/sudoers.d:/etc/sudoers.d:ro
- /home/<your_user>:/home/<your_user>:rw #share your home with write permissions
command: rqtDefining your users UID and GID (as well as other variables) can be done with an .env-file (see Official env-file documentation): UGID=1000:1000and changing above example from user: 1000:1000to user: ${UGID}
Installing Compose
See https://docs.docker.com/compose/install/ Using Compose
Making ROS Compose files
Example compose file version: '2'
networks:
ros:
driver: bridge
services:
ros-master:
image: ros:kinetic-ros-core
command: stdbuf -o L roscore
networks:
- ros
restart: always
talker:
image: ros:kinetic-ros-core
depends_on:
- ros-master
environment:
- "ROS_MASTER_URI=http://ros-master:11311"
command: stdbuf -o L rostopic pub /chatter std_msgs/String "hello" -r 1
networks:
- ros
restart: always
listener:
image: ros:kinetic-ros-core
depends_on:
- ros-master
environment:
- "ROS_MASTER_URI=http://ros-master:11311"
command: stdbuf -o L rostopic echo /chatter
networks:
- ros
restart: always
Running ROS Compose files
docker-compose upor to run in background docker-compose up -d
Stopping ROS Compose files
docker-compose stop
Using GUI's with compose
If you want to have some sort of GUI starting with your compose you will need to add a container as described in Using GUI's with docker. Extending the example above, one way is to share your local X11-server with the container, log in as yourself and use your GUI as you like: viz:
image: osrf/ros:kinetic-desktop-full
container_name: ros_visualizer
depends_on:
- ros-master
networks:
- ros
environment:
- "ROS_MASTER_URI=http://ros-master:11311"
- "DISPLAY"
- "QT_X11_NO_MITSHM=1" #fix some QT bugs
#share your user to the container in order to access your x11
user: 1000:1000 #adapt as needed!
volumes: #share your x11 socket and permissions to the container
- /tmp/.X11-unix:/tmp/.X11-unix:rw
- /etc/group:/etc/group:ro
- /etc/passwd:/etc/passwd:ro
- /etc/shadow:/etc/shadow:ro
- /etc/sudoers:/etc/sudoers:ro
- /etc/sudoers.d:/etc/sudoers.d:ro
- /home/<your_user>:/home/<your_user>:rw #share your home with write permissions
command: rqtDefining your users UID and GID (as well as other variables) can be done with an .env-file (see Official env-file documentation): UGID=1000:1000and changing above example from user: 1000:1000to user: ${UGID}
See https://docs.docker.com/compose/install/ Using Compose
Making ROS Compose files
Example compose file version: '2'
networks:
ros:
driver: bridge
services:
ros-master:
image: ros:kinetic-ros-core
command: stdbuf -o L roscore
networks:
- ros
restart: always
talker:
image: ros:kinetic-ros-core
depends_on:
- ros-master
environment:
- "ROS_MASTER_URI=http://ros-master:11311"
command: stdbuf -o L rostopic pub /chatter std_msgs/String "hello" -r 1
networks:
- ros
restart: always
listener:
image: ros:kinetic-ros-core
depends_on:
- ros-master
environment:
- "ROS_MASTER_URI=http://ros-master:11311"
command: stdbuf -o L rostopic echo /chatter
networks:
- ros
restart: always
Running ROS Compose files
docker-compose upor to run in background docker-compose up -d
Stopping ROS Compose files
docker-compose stop
Using GUI's with compose
If you want to have some sort of GUI starting with your compose you will need to add a container as described in Using GUI's with docker. Extending the example above, one way is to share your local X11-server with the container, log in as yourself and use your GUI as you like: viz:
image: osrf/ros:kinetic-desktop-full
container_name: ros_visualizer
depends_on:
- ros-master
networks:
- ros
environment:
- "ROS_MASTER_URI=http://ros-master:11311"
- "DISPLAY"
- "QT_X11_NO_MITSHM=1" #fix some QT bugs
#share your user to the container in order to access your x11
user: 1000:1000 #adapt as needed!
volumes: #share your x11 socket and permissions to the container
- /tmp/.X11-unix:/tmp/.X11-unix:rw
- /etc/group:/etc/group:ro
- /etc/passwd:/etc/passwd:ro
- /etc/shadow:/etc/shadow:ro
- /etc/sudoers:/etc/sudoers:ro
- /etc/sudoers.d:/etc/sudoers.d:ro
- /home/<your_user>:/home/<your_user>:rw #share your home with write permissions
command: rqtDefining your users UID and GID (as well as other variables) can be done with an .env-file (see Official env-file documentation): UGID=1000:1000and changing above example from user: 1000:1000to user: ${UGID}
Using Compose
Making ROS Compose files
Example compose file version: '2'
networks:
ros:
driver: bridge
services:
ros-master:
image: ros:kinetic-ros-core
command: stdbuf -o L roscore
networks:
- ros
restart: always
talker:
image: ros:kinetic-ros-core
depends_on:
- ros-master
environment:
- "ROS_MASTER_URI=http://ros-master:11311"
command: stdbuf -o L rostopic pub /chatter std_msgs/String "hello" -r 1
networks:
- ros
restart: always
listener:
image: ros:kinetic-ros-core
depends_on:
- ros-master
environment:
- "ROS_MASTER_URI=http://ros-master:11311"
command: stdbuf -o L rostopic echo /chatter
networks:
- ros
restart: always
Running ROS Compose files
docker-compose upor to run in background docker-compose up -d
Stopping ROS Compose files
docker-compose stop
Using GUI's with compose
If you want to have some sort of GUI starting with your compose you will need to add a container as described in Using GUI's with docker. Extending the example above, one way is to share your local X11-server with the container, log in as yourself and use your GUI as you like: viz:
image: osrf/ros:kinetic-desktop-full
container_name: ros_visualizer
depends_on:
- ros-master
networks:
- ros
environment:
- "ROS_MASTER_URI=http://ros-master:11311"
- "DISPLAY"
- "QT_X11_NO_MITSHM=1" #fix some QT bugs
#share your user to the container in order to access your x11
user: 1000:1000 #adapt as needed!
volumes: #share your x11 socket and permissions to the container
- /tmp/.X11-unix:/tmp/.X11-unix:rw
- /etc/group:/etc/group:ro
- /etc/passwd:/etc/passwd:ro
- /etc/shadow:/etc/shadow:ro
- /etc/sudoers:/etc/sudoers:ro
- /etc/sudoers.d:/etc/sudoers.d:ro
- /home/<your_user>:/home/<your_user>:rw #share your home with write permissions
command: rqtDefining your users UID and GID (as well as other variables) can be done with an .env-file (see Official env-file documentation): UGID=1000:1000and changing above example from user: 1000:1000to user: ${UGID}
Making ROS Compose files
Example compose file version: '2'
networks:
ros:
driver: bridge
services:
ros-master:
image: ros:kinetic-ros-core
command: stdbuf -o L roscore
networks:
- ros
restart: always
talker:
image: ros:kinetic-ros-core
depends_on:
- ros-master
environment:
- "ROS_MASTER_URI=http://ros-master:11311"
command: stdbuf -o L rostopic pub /chatter std_msgs/String "hello" -r 1
networks:
- ros
restart: always
listener:
image: ros:kinetic-ros-core
depends_on:
- ros-master
environment:
- "ROS_MASTER_URI=http://ros-master:11311"
command: stdbuf -o L rostopic echo /chatter
networks:
- ros
restart: always
Running ROS Compose files
docker-compose upor to run in background docker-compose up -d
Stopping ROS Compose files
docker-compose stop
Using GUI's with compose
If you want to have some sort of GUI starting with your compose you will need to add a container as described in Using GUI's with docker. Extending the example above, one way is to share your local X11-server with the container, log in as yourself and use your GUI as you like: viz:
image: osrf/ros:kinetic-desktop-full
container_name: ros_visualizer
depends_on:
- ros-master
networks:
- ros
environment:
- "ROS_MASTER_URI=http://ros-master:11311"
- "DISPLAY"
- "QT_X11_NO_MITSHM=1" #fix some QT bugs
#share your user to the container in order to access your x11
user: 1000:1000 #adapt as needed!
volumes: #share your x11 socket and permissions to the container
- /tmp/.X11-unix:/tmp/.X11-unix:rw
- /etc/group:/etc/group:ro
- /etc/passwd:/etc/passwd:ro
- /etc/shadow:/etc/shadow:ro
- /etc/sudoers:/etc/sudoers:ro
- /etc/sudoers.d:/etc/sudoers.d:ro
- /home/<your_user>:/home/<your_user>:rw #share your home with write permissions
command: rqtDefining your users UID and GID (as well as other variables) can be done with an .env-file (see Official env-file documentation): UGID=1000:1000and changing above example from user: 1000:1000to user: ${UGID}
Example compose file version: '2'
networks:
ros:
driver: bridge
services:
ros-master:
image: ros:kinetic-ros-core
command: stdbuf -o L roscore
networks:
- ros
restart: always
talker:
image: ros:kinetic-ros-core
depends_on:
- ros-master
environment:
- "ROS_MASTER_URI=http://ros-master:11311"
command: stdbuf -o L rostopic pub /chatter std_msgs/String "hello" -r 1
networks:
- ros
restart: always
listener:
image: ros:kinetic-ros-core
depends_on:
- ros-master
environment:
- "ROS_MASTER_URI=http://ros-master:11311"
command: stdbuf -o L rostopic echo /chatter
networks:
- ros
restart: always
Running ROS Compose files
docker-compose upor to run in background docker-compose up -d
Stopping ROS Compose files
docker-compose stop
Using GUI's with compose
If you want to have some sort of GUI starting with your compose you will need to add a container as described in Using GUI's with docker. Extending the example above, one way is to share your local X11-server with the container, log in as yourself and use your GUI as you like: viz:
image: osrf/ros:kinetic-desktop-full
container_name: ros_visualizer
depends_on:
- ros-master
networks:
- ros
environment:
- "ROS_MASTER_URI=http://ros-master:11311"
- "DISPLAY"
- "QT_X11_NO_MITSHM=1" #fix some QT bugs
#share your user to the container in order to access your x11
user: 1000:1000 #adapt as needed!
volumes: #share your x11 socket and permissions to the container
- /tmp/.X11-unix:/tmp/.X11-unix:rw
- /etc/group:/etc/group:ro
- /etc/passwd:/etc/passwd:ro
- /etc/shadow:/etc/shadow:ro
- /etc/sudoers:/etc/sudoers:ro
- /etc/sudoers.d:/etc/sudoers.d:ro
- /home/<your_user>:/home/<your_user>:rw #share your home with write permissions
command: rqtDefining your users UID and GID (as well as other variables) can be done with an .env-file (see Official env-file documentation): UGID=1000:1000and changing above example from user: 1000:1000to user: ${UGID}
version: '2'
networks:
ros:
driver: bridge
services:
ros-master:
image: ros:kinetic-ros-core
command: stdbuf -o L roscore
networks:
- ros
restart: always
talker:
image: ros:kinetic-ros-core
depends_on:
- ros-master
environment:
- "ROS_MASTER_URI=http://ros-master:11311"
command: stdbuf -o L rostopic pub /chatter std_msgs/String "hello" -r 1
networks:
- ros
restart: always
listener:
image: ros:kinetic-ros-core
depends_on:
- ros-master
environment:
- "ROS_MASTER_URI=http://ros-master:11311"
command: stdbuf -o L rostopic echo /chatter
networks:
- ros
restart: always
Running ROS Compose files
docker-compose upor to run in background docker-compose up -d
Stopping ROS Compose files
docker-compose stop
Using GUI's with compose
If you want to have some sort of GUI starting with your compose you will need to add a container as described in Using GUI's with docker. Extending the example above, one way is to share your local X11-server with the container, log in as yourself and use your GUI as you like: viz:
image: osrf/ros:kinetic-desktop-full
container_name: ros_visualizer
depends_on:
- ros-master
networks:
- ros
environment:
- "ROS_MASTER_URI=http://ros-master:11311"
- "DISPLAY"
- "QT_X11_NO_MITSHM=1" #fix some QT bugs
#share your user to the container in order to access your x11
user: 1000:1000 #adapt as needed!
volumes: #share your x11 socket and permissions to the container
- /tmp/.X11-unix:/tmp/.X11-unix:rw
- /etc/group:/etc/group:ro
- /etc/passwd:/etc/passwd:ro
- /etc/shadow:/etc/shadow:ro
- /etc/sudoers:/etc/sudoers:ro
- /etc/sudoers.d:/etc/sudoers.d:ro
- /home/<your_user>:/home/<your_user>:rw #share your home with write permissions
command: rqtDefining your users UID and GID (as well as other variables) can be done with an .env-file (see Official env-file documentation): UGID=1000:1000and changing above example from user: 1000:1000to user: ${UGID}
Running ROS Compose files
docker-compose upor to run in background docker-compose up -d
Stopping ROS Compose files
docker-compose stop
Using GUI's with compose
If you want to have some sort of GUI starting with your compose you will need to add a container as described in Using GUI's with docker. Extending the example above, one way is to share your local X11-server with the container, log in as yourself and use your GUI as you like: viz:
image: osrf/ros:kinetic-desktop-full
container_name: ros_visualizer
depends_on:
- ros-master
networks:
- ros
environment:
- "ROS_MASTER_URI=http://ros-master:11311"
- "DISPLAY"
- "QT_X11_NO_MITSHM=1" #fix some QT bugs
#share your user to the container in order to access your x11
user: 1000:1000 #adapt as needed!
volumes: #share your x11 socket and permissions to the container
- /tmp/.X11-unix:/tmp/.X11-unix:rw
- /etc/group:/etc/group:ro
- /etc/passwd:/etc/passwd:ro
- /etc/shadow:/etc/shadow:ro
- /etc/sudoers:/etc/sudoers:ro
- /etc/sudoers.d:/etc/sudoers.d:ro
- /home/<your_user>:/home/<your_user>:rw #share your home with write permissions
command: rqtDefining your users UID and GID (as well as other variables) can be done with an .env-file (see Official env-file documentation): UGID=1000:1000and changing above example from user: 1000:1000to user: ${UGID}
docker-compose upor to run in background docker-compose up -d
Stopping ROS Compose files
docker-compose stop
Using GUI's with compose
If you want to have some sort of GUI starting with your compose you will need to add a container as described in Using GUI's with docker. Extending the example above, one way is to share your local X11-server with the container, log in as yourself and use your GUI as you like: viz:
image: osrf/ros:kinetic-desktop-full
container_name: ros_visualizer
depends_on:
- ros-master
networks:
- ros
environment:
- "ROS_MASTER_URI=http://ros-master:11311"
- "DISPLAY"
- "QT_X11_NO_MITSHM=1" #fix some QT bugs
#share your user to the container in order to access your x11
user: 1000:1000 #adapt as needed!
volumes: #share your x11 socket and permissions to the container
- /tmp/.X11-unix:/tmp/.X11-unix:rw
- /etc/group:/etc/group:ro
- /etc/passwd:/etc/passwd:ro
- /etc/shadow:/etc/shadow:ro
- /etc/sudoers:/etc/sudoers:ro
- /etc/sudoers.d:/etc/sudoers.d:ro
- /home/<your_user>:/home/<your_user>:rw #share your home with write permissions
command: rqtDefining your users UID and GID (as well as other variables) can be done with an .env-file (see Official env-file documentation): UGID=1000:1000and changing above example from user: 1000:1000to user: ${UGID}
or to run in background docker-compose up -d
Stopping ROS Compose files
docker-compose stop
Using GUI's with compose
If you want to have some sort of GUI starting with your compose you will need to add a container as described in Using GUI's with docker. Extending the example above, one way is to share your local X11-server with the container, log in as yourself and use your GUI as you like: viz:
image: osrf/ros:kinetic-desktop-full
container_name: ros_visualizer
depends_on:
- ros-master
networks:
- ros
environment:
- "ROS_MASTER_URI=http://ros-master:11311"
- "DISPLAY"
- "QT_X11_NO_MITSHM=1" #fix some QT bugs
#share your user to the container in order to access your x11
user: 1000:1000 #adapt as needed!
volumes: #share your x11 socket and permissions to the container
- /tmp/.X11-unix:/tmp/.X11-unix:rw
- /etc/group:/etc/group:ro
- /etc/passwd:/etc/passwd:ro
- /etc/shadow:/etc/shadow:ro
- /etc/sudoers:/etc/sudoers:ro
- /etc/sudoers.d:/etc/sudoers.d:ro
- /home/<your_user>:/home/<your_user>:rw #share your home with write permissions
command: rqtDefining your users UID and GID (as well as other variables) can be done with an .env-file (see Official env-file documentation): UGID=1000:1000and changing above example from user: 1000:1000to user: ${UGID}
Stopping ROS Compose files
docker-compose stop
Using GUI's with compose
If you want to have some sort of GUI starting with your compose you will need to add a container as described in Using GUI's with docker. Extending the example above, one way is to share your local X11-server with the container, log in as yourself and use your GUI as you like: viz:
image: osrf/ros:kinetic-desktop-full
container_name: ros_visualizer
depends_on:
- ros-master
networks:
- ros
environment:
- "ROS_MASTER_URI=http://ros-master:11311"
- "DISPLAY"
- "QT_X11_NO_MITSHM=1" #fix some QT bugs
#share your user to the container in order to access your x11
user: 1000:1000 #adapt as needed!
volumes: #share your x11 socket and permissions to the container
- /tmp/.X11-unix:/tmp/.X11-unix:rw
- /etc/group:/etc/group:ro
- /etc/passwd:/etc/passwd:ro
- /etc/shadow:/etc/shadow:ro
- /etc/sudoers:/etc/sudoers:ro
- /etc/sudoers.d:/etc/sudoers.d:ro
- /home/<your_user>:/home/<your_user>:rw #share your home with write permissions
command: rqtDefining your users UID and GID (as well as other variables) can be done with an .env-file (see Official env-file documentation): UGID=1000:1000and changing above example from user: 1000:1000to user: ${UGID}
docker-compose stop
Using GUI's with compose
If you want to have some sort of GUI starting with your compose you will need to add a container as described in Using GUI's with docker. Extending the example above, one way is to share your local X11-server with the container, log in as yourself and use your GUI as you like: viz:
image: osrf/ros:kinetic-desktop-full
container_name: ros_visualizer
depends_on:
- ros-master
networks:
- ros
environment:
- "ROS_MASTER_URI=http://ros-master:11311"
- "DISPLAY"
- "QT_X11_NO_MITSHM=1" #fix some QT bugs
#share your user to the container in order to access your x11
user: 1000:1000 #adapt as needed!
volumes: #share your x11 socket and permissions to the container
- /tmp/.X11-unix:/tmp/.X11-unix:rw
- /etc/group:/etc/group:ro
- /etc/passwd:/etc/passwd:ro
- /etc/shadow:/etc/shadow:ro
- /etc/sudoers:/etc/sudoers:ro
- /etc/sudoers.d:/etc/sudoers.d:ro
- /home/<your_user>:/home/<your_user>:rw #share your home with write permissions
command: rqtDefining your users UID and GID (as well as other variables) can be done with an .env-file (see Official env-file documentation): UGID=1000:1000and changing above example from user: 1000:1000to user: ${UGID}
Using GUI's with compose
If you want to have some sort of GUI starting with your compose you will need to add a container as described in Using GUI's with docker. Extending the example above, one way is to share your local X11-server with the container, log in as yourself and use your GUI as you like: viz:
image: osrf/ros:kinetic-desktop-full
container_name: ros_visualizer
depends_on:
- ros-master
networks:
- ros
environment:
- "ROS_MASTER_URI=http://ros-master:11311"
- "DISPLAY"
- "QT_X11_NO_MITSHM=1" #fix some QT bugs
#share your user to the container in order to access your x11
user: 1000:1000 #adapt as needed!
volumes: #share your x11 socket and permissions to the container
- /tmp/.X11-unix:/tmp/.X11-unix:rw
- /etc/group:/etc/group:ro
- /etc/passwd:/etc/passwd:ro
- /etc/shadow:/etc/shadow:ro
- /etc/sudoers:/etc/sudoers:ro
- /etc/sudoers.d:/etc/sudoers.d:ro
- /home/<your_user>:/home/<your_user>:rw #share your home with write permissions
command: rqtDefining your users UID and GID (as well as other variables) can be done with an .env-file (see Official env-file documentation): UGID=1000:1000and changing above example from user: 1000:1000to user: ${UGID}
If you want to have some sort of GUI starting with your compose you will need to add a container as described in Using GUI's with docker. Extending the example above, one way is to share your local X11-server with the container, log in as yourself and use your GUI as you like: viz:
image: osrf/ros:kinetic-desktop-full
container_name: ros_visualizer
depends_on:
- ros-master
networks:
- ros
environment:
- "ROS_MASTER_URI=http://ros-master:11311"
- "DISPLAY"
- "QT_X11_NO_MITSHM=1" #fix some QT bugs
#share your user to the container in order to access your x11
user: 1000:1000 #adapt as needed!
volumes: #share your x11 socket and permissions to the container
- /tmp/.X11-unix:/tmp/.X11-unix:rw
- /etc/group:/etc/group:ro
- /etc/passwd:/etc/passwd:ro
- /etc/shadow:/etc/shadow:ro
- /etc/sudoers:/etc/sudoers:ro
- /etc/sudoers.d:/etc/sudoers.d:ro
- /home/<your_user>:/home/<your_user>:rw #share your home with write permissions
command: rqtDefining your users UID and GID (as well as other variables) can be done with an .env-file (see Official env-file documentation): UGID=1000:1000and changing above example from user: 1000:1000to user: ${UGID}
Extending the example above, one way is to share your local X11-server with the container, log in as yourself and use your GUI as you like: viz:
image: osrf/ros:kinetic-desktop-full
container_name: ros_visualizer
depends_on:
- ros-master
networks:
- ros
environment:
- "ROS_MASTER_URI=http://ros-master:11311"
- "DISPLAY"
- "QT_X11_NO_MITSHM=1" #fix some QT bugs
#share your user to the container in order to access your x11
user: 1000:1000 #adapt as needed!
volumes: #share your x11 socket and permissions to the container
- /tmp/.X11-unix:/tmp/.X11-unix:rw
- /etc/group:/etc/group:ro
- /etc/passwd:/etc/passwd:ro
- /etc/shadow:/etc/shadow:ro
- /etc/sudoers:/etc/sudoers:ro
- /etc/sudoers.d:/etc/sudoers.d:ro
- /home/<your_user>:/home/<your_user>:rw #share your home with write permissions
command: rqtDefining your users UID and GID (as well as other variables) can be done with an .env-file (see Official env-file documentation): UGID=1000:1000and changing above example from user: 1000:1000to user: ${UGID}
Defining your users UID and GID (as well as other variables) can be done with an .env-file (see Official env-file documentation): UGID=1000:1000and changing above example from user: 1000:1000to user: ${UGID}
and changing above example from user: 1000:1000to user: ${UGID}
to user: ${UGID}
Wiki: docker/Tutorials/Compose (last edited 2022-03-08 20:56:44 by MahmoodHikmet)