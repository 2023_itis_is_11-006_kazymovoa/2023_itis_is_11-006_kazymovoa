import numpy as np
import pandas as pd

from invert_index_maker import doc_id2word_list

import sys


class TfIdfSearchEngine:
    def __init__(self, tf_idf_file, treshold=0.001):
        self.__data: pd.DataFrame = self.__load_tf_idf(tf_idf_file)

    @staticmethod
    def __load_tf_idf(tf_idf_file: str) -> pd.DataFrame:
        return pd.read_csv(tf_idf_file)

    def vectorize_document(self, tokens: list[str]) -> np.ndarray:
        return self.__data.loc[self.__data['Term'].isin(tokens)].drop('Term', axis=1).mean().to_numpy()

    def search(self, query: str):
        doc = query.split()
        vec = self.vectorize_document(doc)
        candidates = sorted([(i+1, val) for i, val in enumerate(vec)], key=lambda x: -x[1])
        return candidates

class Log:
    def __init__(self, filename: str):
        self.filename = filename
        self.data = ""

    def log(self, text: str):
        self.data += text + "\n"

    def save(self):
        with open(self.filename, 'w', encoding="utf-8") as f:
            f.write(self.data)


if __name__ == '__main__':
    log = Log(sys.argv[1])
    if len(sys.argv) > 2:
        se = TfIdfSearchEngine("..\\4\\tfidf_tables\\tfidf.csv")
        query = ' '.join(sys.argv[2:])
        log.log("Результаты поиска:")
        log.log(f"Запрос: '{query}'\nДокументы: \n" + "\n".join(map(lambda x: f"Документ {x[0]}, сходство - {x[1]}", se.search(query))))
    else:
        log.log("Пожалуйста, введите запрос в виде аргументов командной строки.")
    log.save()