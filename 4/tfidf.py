import math

import pandas as pd

from tqdm import tqdm
from invert_index_maker import word_list2doc_id, doc_id2word_list

def get_idfs(doc2words, n):
    word_list = word_list2doc_id(doc2words)
    words = []
    idfs = []
    for word, ids in word_list.items():
        words.append(word)
        idfs.append(math.log2(n/len(ids)))
    return words, idfs


def main():
    n = 100
    doc2words = doc_id2word_list(n)
    words, idfs = get_idfs(doc2words, n)
    tf_dict = {"Term": words}
    idf_dict = {"Term": words, "IDF": [round(x, 6) for x in idfs]}
    tfidf_dict = {"Term": words}
    for d_id, wordlist in tqdm(doc2words.items()):
        tfs = []
        for w in words:
            tf = wordlist.count(w) / max(1, len(wordlist))
            tfs.append(tf)
        tf_dict[d_id] = [round(x, 6) for x in tfs]
        tfidf_dict[d_id] = [round(x*y, 6) for x, y in zip(tfs, idfs)]

    pd.DataFrame.from_dict(tf_dict).to_csv(f"./tfidf_tables/tf.csv", sep=",", index=False)
    pd.DataFrame.from_dict(idf_dict).to_csv(f"./tfidf_tables/idf.csv", sep=",", index=False)
    pd.DataFrame.from_dict(tfidf_dict).to_csv(f"./tfidf_tables/tfidf.csv", sep=",", index=False)


if __name__ == "__main__":
    main()
