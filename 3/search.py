import sys

from search_engine import SearchEngine

if __name__ == '__main__':
    if len(sys.argv) > 1:
        se = SearchEngine()
        query = ' '.join(sys.argv[1:])
        print("Результаты поиска:")
        print(f"Запрос: '{query}' - Документы: {se.search(query)}")
    else:
        print("Пожалуйста, введите запрос в виде аргументов командной строки.")