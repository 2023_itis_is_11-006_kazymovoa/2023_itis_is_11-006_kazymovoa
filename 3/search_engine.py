from invert_index_maker import word_list2doc_id, doc_id2word_set


class SearchEngine:
    def __init__(self):
        self.inverted_index = word_list2doc_id(doc_id2word_set(100))
        self._all = set(range(1, 101))

    def _not(self, ids: set):
        return self._all.difference(ids)

    def search(self, query: str):
        query_parts = query.split()
        res = self._all
        curr_op = "&"
        for part in query_parts:
            if part in ["&", "|"]:
                curr_op = part
            else:
                inverted = False
                if part[0] == "!":
                    inverted = True
                    part = part[1:]
                try:
                    selection = self.inverted_index[part]
                except KeyError:
                    selection = set()
                if inverted:
                    selection = self._not(selection)

                if curr_op == "&":
                    res = res.intersection(selection)
                else:
                    res = res.union(selection)
        return sorted(list(res))
