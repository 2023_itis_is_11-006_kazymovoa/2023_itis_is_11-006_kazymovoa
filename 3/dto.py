def toJsonSerializable(_in):
    return {k: sorted(list(v)) for k, v in _in.items()}
