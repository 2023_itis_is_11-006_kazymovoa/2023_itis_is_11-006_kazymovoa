import re

import nltk

nltk.download("stopwords")

from pymorphy2 import MorphAnalyzer
from nltk.corpus import stopwords
from tqdm import tqdm

morph = MorphAnalyzer()
stopwords = stopwords.words("russian")


def tokenize(text):
    return re.findall(r"\b[А-Яа-я]+(?:-[А-Яа-я]+)*\b", text)


def clean_tokens(tokens):
    return list(filter(lambda token: token not in stopwords, tokens))


def lemmatize_text(text):
    tokens = clean_tokens(tokenize(text))
    lemmas = [morph.parse(token)[0].normal_form for token in tokens]
    return ' '.join(lemmas)


# legacy
def preprocess_document(source_fname, target_fname):
    with open(source_fname, 'r') as f:
        lines_to_write = list(map(lemmatize_text, f.readlines()))
    with open(target_fname, 'w') as f:
        f.writelines(lines_to_write)


def preprocess_document_as_full_text(source_fname, target_fname):
    with open(source_fname, 'r', encoding="utf-8") as f:
        to_write = lemmatize_text(f.read())
    with open(target_fname, 'w', encoding="utf-8") as f:
        f.write(to_write)


def main():
    n = 100
    for i in tqdm(range(1, n + 1)):
        source_fname = f'../1/pages/{i}.txt'
        target_fname = f'./cleaned_docs/{i}.txt'
        preprocess_document_as_full_text(source_fname, target_fname)


if __name__ == '__main__':
    main()
